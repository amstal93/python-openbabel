# Docker Images for Python with Openbabel

Docker images for Python 3.6+ versions that include the openbabel library built from sources as well as corresponding Python bindings.

You shouldn't need to interact with this repository. You can simple `docker
pull midnighter/python-openbabel:<tag>`.

## Tags

* `3.6-2.4.1`
* `3.7-2.4.1`
* `3.8-rc-2.4.1`

## Copyright

* Copyright © 2019, Moritz E. Beber. All rights reserved.
* Distributed under the liberal [new BSD license](LICENSE).
